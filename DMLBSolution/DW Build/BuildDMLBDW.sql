-- DMLBDW developed and written by David Bray
--Created Date 10/11/2017
--This is the build script for the Data Warehouse to the OLTP DMLB

--Creating the data warehouse if it does not exist yet
USE [master]
GO
IF NOT EXISTS(SELECT * FROM sys.databases
	WHERE name = N'DMLBDW')
	CREATE DATABASE DMLBDW
GO
USE DMLBDW

--DROP TABLE SECTION
--Dropping the tables in reverse order (Foreign Keys will be dropped first)
IF EXISTS(
	SELECT *
	FROM sys.tables
	WHERE name = N'FactRunsScored'
       )
	DROP TABLE FactRunsScored;

IF EXISTS(
	SELECT *
	FROM sys.tables
	WHERE name = N'DimDate'
       )
	DROP TABLE DimDate;

IF EXISTS(
	SELECT *
	FROM sys.tables
	WHERE name = N'DimPersonnel'
       )
	DROP TABLE DimPersonnel;

IF EXISTS(
	SELECT *
	FROM sys.tables
	WHERE name = N'DimTeam'
       )
	DROP TABLE DimTeam;

IF EXISTS(
	SELECT *
	FROM sys.tables
	WHERE name = N'DimAttribute'
      )
	DROP TABLE DimAttribute;

IF EXISTS(
	SELECT *
	FROM sys.tables
	WHERE name = N'DimGame'
      )
	DROP TABLE DimGame;


--CREATE TABLE SECTION
--Tables are created in the opposite order of which they were dropped
--This ensures that primary keys are created before they are used as a foreign key in another table (The fact table)

CREATE TABLE DimAttribute
	(Attribute_SK		INT IDENTITY(1,1) CONSTRAINT pk_Attribute PRIMARY KEY,
	Attribute_AK		INT NOT NULL,
	DayOrNight		NVARCHAR(10) NOT NULL,
	GameType		NVARCHAR(45) NOT NULL,
	--HomeTeamRuns	INT NOT NULL,
	--VisitorRuns		INT NOT NULL,
	--VisitorHits		INT NOT NULL,
	--HomeHits		INT NOT NULL,
	--GameLength_Outs INT NOT NULL,
	--VisitorRBIs		INT NOT NULL,
	--HomeRBIs		INT NOT NULL,
	Stadium			NVARCHAR(100) NOT NULL
	);

CREATE TABLE DimTeam
	(Team_SK	INT IDENTITY(1,1) CONSTRAINT pk_Team PRIMARY KEY,
	Team_AK		NVARCHAR(3) NOT NULL,
	League		NVARCHAR(25) NOT NULL,
	Division	NVARCHAR(35) NOT NULL,
	TeamName	NVARCHAR(15) NOT NULL
	);

CREATE TABLE DimPersonnel
	(Personnel_SK	INT IDENTITY(1,1) CONSTRAINT pk_Personnel PRIMARY KEY,
	Personnel_AK	INT NOT NULL,
	PlayerName		NVARCHAR (100) NOT NULL, --This is a concatenated value of lastname and firstname
	ManagerName		NVARCHAR (100) NOT NULL, --Also a concatenated name from lastname and firstname
	UmpireName		NVARCHAR (100) NOT NULL, --Also a concatenated name from lastname and firstname
	PlayerPosition	VARCHAR(18) NOT NULL,
	StartDate		DateTime,
	EndDate			DateTime,
	);

--DimDate table code is courtesy of Amy Phillips.
--This table will not match what is listed in my Visio ERD. The columns here are much more expansive than I initially thought
--Some of these do look to be quite useful however when it comes to what I am analyzing.
CREATE TABLE DimDate
	(	Date_SK INT PRIMARY KEY, 
		Date DATETIME,
		FullDate NVARCHAR(10),-- Date in MM-dd-yyyy format
		DayOfMonth INT, -- Field will hold day number of Month
		DayName VARCHAR(9), -- Contains name of the day, Sunday, Monday 
		DayOfWeek INT,-- First Day Sunday=1 and Saturday=7
		DayOfWeekInMonth INT, -- 1st Monday or 2nd Monday in Month
		DayOfWeekInYear INT,
		DayOfQuarter INT,
		DayOfYear INT,
		WeekOfMonth INT,-- Week Number of Month 
		WeekOfQuarter INT, -- Week Number of the Quarter
		WeekOfYear INT,-- Week Number of the Year
		Month INT, -- Number of the Month 1 to 12{}
		MonthName VARCHAR(9),-- January, February etc
		MonthOfQuarter INT,-- Month Number belongs to Quarter
		Quarter CHAR(2),
		QuarterName VARCHAR(9),-- First,Second..
		Year INT,-- Year value of Date stored in Row
		YearName CHAR(7), -- CY 2015,CY 2016
		MonthYear CHAR(10), -- Jan-2016,Feb-2016
		MMYYYY INT,
		FirstDayOfMonth DATE,
		LastDayOfMonth DATE,
		FirstDayOfQuarter DATE,
		LastDayOfQuarter DATE,
		FirstDayOfYear DATE,
		LastDayOfYear DATE,
		IsHoliday BIT,-- Flag 1=National Holiday, 0-No National Holiday
		IsWeekday BIT,-- 0=Week End ,1=Week Day
		Holiday VARCHAR(50),--Name of Holiday in US
		Season VARCHAR(10)--Name of Season
	);

CREATE TABLE FactRunsScored
	(Date_SK INT CONSTRAINT  fk_Date FOREIGN KEY REFERENCES DimDate (Date_SK),
	Personnel_SK INT CONSTRAINT fk_Personnel FOREIGN KEY REFERENCES DimPersonnel (Personnel_SK),
	Team_SK INT CONSTRAINT fk_Team FOREIGN KEY REFERENCES DimTeam (Team_SK),
	Attribute_SK INT CONSTRAINT fk_Game FOREIGN KEY REFERENCES DimAttribute(Attribute_SK),
	Attendance INT NOT NULL,
	GameTime_Min INT NOT NULL,
	RunsScored INT NOT NULL, -- This is a derived value of Home team runs plus visitor runs
	PRIMARY KEY (Date_SK, Personnel_SK, Team_SK, Attribute_SK)
	);