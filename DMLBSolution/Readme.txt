--The project was done as part of the Capstone class at DU to show proficiency in Data Analysis

This project is designed to show an ETL project
It is broken down into 5 different phases
Phase 1 is building the initial OLTP database to capture data
Phase 2 is builting the datawarehouse that will contain 5 tables in a star schema for analysis (build script provided)
Phase 3 is running the ETL process from the database to the datawarehouse
Phase 4 is building some reports and dashboards in PowerBI
Phase 5 is an analysis of the data in SSIS then presented in an XLSM file

The platforms used to create these projects was visual studio, SQL Server (SSMS, SSAS, SSIS), Excel, and PowerBI